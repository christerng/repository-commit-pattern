#!/usr/bin/python3
'''
Christopher Terence Ng
Fetches author, email, time, for each commit on master in a given repo
'''
import os
import requests
from requests.auth import HTTPBasicAuth
from datetime import timedelta, datetime

URL_BASE = 'https://api.github.com/repos'


def format_url(args):
    '''
    @param str[] system arguments
    @return str(HTTP request URL)
    '''
    owner, repo = args[1:3]
    return '/'.join([URL_BASE, owner, repo, 'commits'])


def format_params(args):
    '''
    @param str[] system arguments
    @return {start_date, end_date} in GitHub API format 
    '''
    start_date, end_date = args[3:]

    try:
        start_date = datetime.strptime(start_date, '%Y%m%d')
        end_date = datetime.strptime(end_date, '%Y%m%d')
    except ValueError:
        print('Dates must follow YYYYMMDD format', end='\n\n')
        return

    start_date = datetime.strftime(start_date, '%Y-%m-%d')
    end_date = end_date + timedelta(days=1)  # So that end date is inclusive
    end_date = datetime.strftime(end_date, '%Y-%m-%d')
    return {'since': start_date, 'until': end_date}


def unpack_json(response):
    '''
    @param str(HTTP response)
    @return commit[]
    '''
    return [commit['commit']['author'] for commit in response.json()]

def generate_commits(url, params):
    '''
    @param str(HTTP request URL)
    @param str(HTTP request parameters)
    @yield commit[]
    '''
    if os.environ.get('username') and os.environ.get('password'):
        print('Using environment credentials')
        response = requests.get(
                url, 
                params, 
                auth=HTTPBasicAuth(
                    os.environ['username'],
                    os.environ['password']
                    )
                )
    else:
        response = requests.get(url, params)

    if response.status_code != 200:
        print(f'HTTP Status Code {response.status_code}', end='\n\n')
        return

    for commit in unpack_json(response):
        yield commit

    if response.links.get('next'):
        next_url = response.links['next']['url']
        yield from generate_commits(next_url, params)  # Yes, it's recursive


def list_commits(args):
    url = format_url(args)
    params = format_params(args)

    if not (url and params):
        return 

    return [commit for commit in generate_commits(url, params)]

