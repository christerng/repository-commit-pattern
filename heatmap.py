#!/usr/bin/python3
'''
Christopher Terence Ng
Reads commits.db and returns table of commits by day of week, three-hourly bin
'''
import sqlite3
import datetime
import numpy


def read_db():
    conn = sqlite3.connect('./commits.db')
    cur = conn.cursor()
    cur.execute('SELECT datetime FROM commits;')
    rows = cur.fetchall()
    conn.close()

    return rows

def three_hour_bin(time):
    if datetime.time(hour=0) <= time < datetime.time(hour=3):
        return 0
    elif datetime.time(hour=3) <= time < datetime.time(hour=6):
        return 1
    elif datetime.time(hour=6) <= time < datetime.time(hour=9):
        return 2
    elif datetime.time(hour=9) <= time < datetime.time(hour=12):
        return 3
    elif datetime.time(hour=12) <= time < datetime.time(hour=15):
        return 4
    elif datetime.time(hour=15) <= time < datetime.time(hour=18):
        return 5
    elif datetime.time(hour=18) <= time < datetime.time(hour=21):
        return 6
    return 7


def make_table(rows):
    heatmap = numpy.zeros((8, 7))

    for row in rows:
        date_dt = datetime.datetime.strptime(row[0], '%Y-%m-%dT%H:%M:%SZ')
        day_of_week = date_dt.weekday()
        time_dt = date_dt.time()
        time_bin = three_hour_bin(time_dt)

        heatmap[time_bin][day_of_week] += 1 

    return heatmap

if __name__ == '__main__':
    rows = read_db()
    heatmap = make_table(rows)
    print(heatmap)

