#!/usr/bin/python3
'''
Christopher Terence Ng
Loads author, email, time for each commit into DB
'''
import sqlite3

DB_NAME = 'commits.db'


def create_tables():
    conn = sqlite3.connect(DB_NAME, detect_types=sqlite3.PARSE_DECLTYPES)
    with conn as c:
        c.execute(
            '''CREATE TABLE IF NOT EXISTS commits (
               author TEXT,
               email TEXT,
               datetime TIMESTAMP);'''
                 ) 
    conn.close()


def insert_rows(commits):
    conn = sqlite3.connect(DB_NAME, detect_types=sqlite3.PARSE_DECLTYPES)
    with conn as c:
        c.executemany('INSERT INTO commits VALUES (?, ?, ?)', commits)
    conn.close()

