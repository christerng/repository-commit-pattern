#!/usr/bin/python3
'''
Christopher Terence Ng
Requests data via API and loads data into DB
'''
import sys
import request
import load


def main():
    if len(sys.argv) != 5:
        print(f'USAGE: {__file__} <owner_name> <repo_name> '
               '<start_date_inclusive> <end_date_inclusive>', end='\n\n')
        return

    commits = request.list_commits(sys.argv)
    # unpack dictionaries into tuples
    commits = [(commit['name'],
                commit['email'],
                commit['date']) for commit in commits]

    load.create_tables()
    load.insert_rows(commits)


if __name__ == '__main__':
    main()

