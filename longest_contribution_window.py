#!/usr/bin/python3
'''
Christopher Terence Ng
Reads commits.db and returns author, distance with longest contribution window
'''
import sqlite3


def read_db():
    conn = sqlite3.connect('./commits.db')
    cur = conn.cursor()
    cur.execute('SELECT author FROM commits;')
    rows = cur.fetchall()
    conn.close()

    return rows


def longest_window(rows):
    '''
    @param author[]
    @return str(author) author with the longest contribution window
    '''
    author_windows = {}
    max_distance = 0
    contributor = ''

    for position, author in enumerate(rows):
        if author not in author_windows:
            author_windows[author] = position 
        else:
            if (position - author_windows[author]) > max_distance:
                max_distance = position - author_windows[author]
                contributor = author

    return contributor


if __name__ == '__main__':
    rows = read_db()
    author = longest_window(rows)
    print(author)

