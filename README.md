How to execute the code
  1. Ensure that python3 is installed
  2. Run app.py using "./app.py <owner_name> <repo_name> <start_date_inclusive> <end_date_inclusive>

Usage of any dependencies
  1. Numpy was installed using "pip install numpy" and was used to generate the heatmap
  2. DBeaver was used to run the query found in top3Authors 

Document queries or scripts to answer the 3 questions
  1. Using DBeaver, open a new SQL Editor and enter the query found in top3Authors, which returns the top 3 authors in a given time period
  2. Run longest_contribution_window using "./longest_contribution_window.py", which returns the author with the longest contribution window 
  3. Run heatmap using "./heatmap.py", which returns an 8x7 matrix of commit count, with each row representing a 3-hour bin and each column representing a day of the week 
   
